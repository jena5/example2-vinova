import React from 'react';
import {TextInputProps} from 'react-native';
import styled from 'styled-components/native';

import DefaultTheme from '@assets/theme';
import Text from '@components/Text';

const StyleTextInput = styled.TextInput`
  height: 44px;
  border: 1px solid ${DefaultTheme.colors.textPrimary};
  flex-grow: 1;
  border-radius: 8px;
  padding: 0px 8px;
`;

const StyledView = styled.View`
  padding-bottom: 8px;
`;

const StyledTextLight = styled(Text[100].ThinText12)`
  font-weight: 700;
  margin-bottom: 4px;
`;

const StyledTextError = styled(Text[100].ThinText)`
  font-weight: 700;
  margin-top: 4px;
`;

const Component = ({
  errorMessage,
  label,
  ...props
}: {errorMessage?: string; label: string} & TextInputProps) => {
  return (
    <StyledView>
      <StyledTextLight>{label}</StyledTextLight>
      <StyleTextInput {...props} />
      {errorMessage ? (
        <StyledTextError color={DefaultTheme.colors.error}>
          {errorMessage}
        </StyledTextError>
      ) : null}
    </StyledView>
  );
};

Component.default = {};

export default Component;

import * as yup from 'yup';
import i18n from '@locales/index';
import {isValidateEmail} from './common';

const userSchema = yup.object().shape({
  email: yup
    .string()
    .test('isValidateEmail', i18n.t('user.errors.email'), value => {
      if (value) {
        return isValidateEmail(value);
      }
      return true;
    }),
  username: yup
    .string()
    .test('isValidateUsername', i18n.t('user.errors.username'), value => {
      if (value) {
        return /[a-zA-Z0-9_]/g.test(value);
      }
      return true;
    }),
});

export default userSchema;

import React from 'react';
import Text from '@components/Text';
import VStack from '@components/VerticalStack';
import HStack from '@components/HorizontalStack';
import Spacer from '@components/Spacer';
import TextInput from '@components/TextInput';
import i18n from '@locales/index';
import styled from 'styled-components/native';
import View from '@components/View';
import {useSelector} from 'react-redux';
import {getUser} from '@redux/user/reducer';

import {useDispatch} from 'react-redux';
import DropShadow from 'react-native-drop-shadow';
import { lightColors } from '@assets/theme';
const UserInfoScreen = () => {
  const user = useSelector(getUser);

  return (
    <StyledWrapped>
      <StyledDropShadow>
        <StyledView>
          <StyledWrappedText>
            <StyledGrow1>
              <StyledWelcomeLight>{i18n.t('user.email')}</StyledWelcomeLight>
            </StyledGrow1>
            <StyledGrow1>
              <StyledWelcomeThinText>{user.email}</StyledWelcomeThinText>
            </StyledGrow1>
          </StyledWrappedText>
          <StyledWrappedText>
            <StyledGrow1>
              <StyledWelcomeLight>{i18n.t('user.username')}</StyledWelcomeLight>
            </StyledGrow1>
            <StyledGrow1>
              <StyledWelcomeThinText>{user.username}</StyledWelcomeThinText>
            </StyledGrow1>
          </StyledWrappedText>
          <StyledWrappedText>
            <StyledGrow1>
              <StyledWelcomeLight>{i18n.t('user.age')}</StyledWelcomeLight>
            </StyledGrow1>
            <StyledGrow1>
              <StyledWelcomeThinText>{user.age}</StyledWelcomeThinText>
            </StyledGrow1>
          </StyledWrappedText>
        </StyledView>
      </StyledDropShadow>
    </StyledWrapped>
  );
};

const StyledWrapped = styled(View)`
  padding: 20% 20px 0px;
  flex: 1;
`;

const StyledView = styled.View`
  width: 100%;
  border: 1px solid ${lightColors.textPrimary};
  border-radius: 16px;
  padding: 16px;
`;

const StyledGrow1 = styled.View`
  flex: 1;
`;
const StyledWrappedText = styled.View`
  min-height: 44px;
  flex-direction: row;
  justify-content: center;
`;

const StyledWelcomeThinText = styled.Text`
  font-weight: 300;
  font-size: 16px;
  line-height: 18px;
  color: ${lightColors.textPrimary};
`;

const StyledWelcomeLight = styled.Text`
  font-weight: 700;
  font-size: 16px;
  line-height: 18px;
  color: ${lightColors.textPrimary};
`;

const StyledDropShadow = styled(DropShadow)`
shadow-color: #000;
shadow-offset: {
  width: 0;
  height: 0;
},
shadow-opacity: 1;
shadow-radius: 5;`;
export default UserInfoScreen;

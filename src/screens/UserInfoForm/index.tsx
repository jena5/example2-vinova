import React, {useCallback} from 'react';
import styled from 'styled-components/native';
import {useForm, Controller} from 'react-hook-form';
import {useDispatch} from 'react-redux';
import {Keyboard} from 'react-native';

import VStack from '@components/VerticalStack';
import TextInput from '@components/TextInput';
import i18n from '@locales/index';
import {Slice as UserSlide} from '@redux/user/reducer';
import {yupResolver} from '@hookform/resolvers/yup';
import userSchema from '@utils/yup';
import Text from '@components/Text';
import DefaultTheme from '@assets/theme';

const defaultValues = {
  username: '',
  email: '',
  age: '',
};

const UserInfoForm = () => {
  const {handleSubmit, control} = useForm({
    defaultValues,
    resolver: yupResolver(userSchema),
  });
  const dispatch = useDispatch();

  const onSubmit = useCallback(
    data => {
      Keyboard.dismiss();
      dispatch(UserSlide.actions.setUser(data));
    },
    [dispatch],
  );

  return (
    <StyledWrapped>
      <Controller
        control={control}
        name="email"
        render={({field: {onChange}, formState: {errors}}) => {
          return (
            <TextInput
              onChangeText={onChange}
              errorMessage={errors.email?.message}
              label={i18n.t('user.email')}
            />
          );
        }}
      />
      <Controller
        control={control}
        name="username"
        render={({field: {onChange}, formState: {errors}}) => {
          return (
            <TextInput
              onChangeText={onChange}
              errorMessage={errors.username?.message}
              label={i18n.t('user.username')}
            />
          );
        }}
      />
      <Controller
        control={control}
        name="age"
        render={({field: {onChange}}) => {
          return (
            <TextInput
              onChangeText={onChange}
              label={i18n.t('user.age')}
              keyboardType="number-pad"
            />
          );
        }}
      />
      <StyledButton onPress={handleSubmit(onSubmit)}>
        <StyledButtonText color={DefaultTheme.colors.backgroundPrimary}>
          {i18n.t('user.submit')}
        </StyledButtonText>
      </StyledButton>
    </StyledWrapped>
  );
};

const StyledWrapped = styled(VStack).attrs(() => ({
  alignment: 'flex-start',
}))`
  padding: 50px 20px 0px;
`;

const StyledButton = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  margin-top: 16px;
  padding: 16px;
  background-color: ${DefaultTheme.colors.green};
  border-radius: 8px;
`;

const StyledButtonText = styled(Text[100].ThinText12)`
  font-weight: 700;
`;
export default UserInfoForm;

// src/features/todo/todoSlice.js
import {createSlice, createAction, PayloadAction} from '@reduxjs/toolkit';

import type {RootState} from '../../index';
// Define a type for the slice state

export interface SliceState {
  username: string;
  email: string;
  age: string;
}

// Define the initial state using that type
const initialState = {
  username: '',
  email: '',
  age: '',
} as SliceState;

export const Slice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state: SliceState, {payload}: {payload: SliceState}) => {
      state.username = payload.username;
      state.email = payload.email;
      state.age = payload.age;
    },
  },
});
// actions
export const {setUser} = Slice.actions;

// selectors
export const getUser = (state: RootState) => state.user;

export default Slice.reducer;
